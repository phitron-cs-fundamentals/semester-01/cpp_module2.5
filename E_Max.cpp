#include <iostream>
#include <algorithm> //min & max header file
using namespace std;
int main()
{
    int N;
    cin >> N;
    int a[N];
    for (int i = 0; i < N; i++)
    {
        cin >> a[i];
    }

    cout << *max_element(a, a + N) << endl;

    return 0;
}